const DEV = process.env.NODE_ENV === 'development'

export const CDN_ROOT = DEV
  ? 'http://localhost:9999/'
  : 'https://e4r8s2u5.map2.ssl.hwcdn.net/'

export const API_URL = DEV
  ? 'http://localhost:9080/'
  : `https://${process.env.APP_VERSION}-dot-api-dot-madfingergames-web.appspot.com/`
