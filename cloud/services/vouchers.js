// API DOCS:
// https://docs.google.com/document/d/1LU_s8xpXi-SPwhDwOMFO64OFVIkpQSA-RI19WV66tzg/

import createRequest from './core'

export default class VouchersService {
  constructor (credentials, projectId, cloudVer) {
    this.request = (params) =>
      createRequest({
        ...params,
        service: 'vouchers',
        credentials,
        projectId,
        cloudVer
      })
  }

  listVoucherSets () {
    return this.request({
      endpoint: '[PROJECT_ID]'
    })
  }

  getVoucherSetInfo (voucherSet) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}`
    })
  }

  getVoucherSetInfos (voucherSet) {
    return this.request({
      endpoint: '[PROJECT_ID]',
      query: {
        id: voucherSet
      },
      method: 'post'
    })
  }

  createVoucherSet (voucherSet) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}`,
      method: 'post'
    })
  }

  editVoucherSet (voucherSet, data) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}`,
      method: 'put',
      body: data
    })
  }

  deleteVoucherSet (voucherSet) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}`,
      method: 'delete'
    })
  }

  listVoucherSetCoupons (voucherSet, cursor) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}/list`,
      query: {cursor}
    })
  }

  generateCoupon (voucherSet) {
    return this.request({
      endpoint: `[PROJECT_ID]/${voucherSet}/coupon`,
      method: 'post'
    })
  }
}
