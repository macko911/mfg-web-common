// DOCS: https://docs.google.com/document/d/1gLS59lTFcOI28J1XBcUrRiIHvlSVOH3KiBh-rruzCR0/edit#

import createRequest from './core'

export default class MailService {
  constructor (credentials, projectId, cloudVer) {
    this.request = (params) =>
      createRequest({
        ...params,
        service: 'mail',
        credentials,
        projectId,
        cloudVer
      })
  }

  sendEmail (params) {
    const {to, from, subject, message, replyTo, nameTo, nameFrom} = params

    if (!to || !subject || !message) {
      return {err: 'Missing required param "to", "subject" or "message".'}
    }
    return this.request({
      endpoint: 'send/[PROJECT_ID]',
      query: {
        from,
        to,
        subject,
        replyto: replyTo || from,
        nameto: nameTo,
        namefrom: nameFrom
      },
      body: message,
      method: 'post',
      contentType: 'text/html'
    })
  }
}
