// DOCS: https://docs.google.com/document/d/14YcyBpNhYe8oR_ROUE2JWSgSbp8tJyFz5MhROhWg4A0/edit

import createRequest from './core'

export default class GuildsService {
  constructor (credentials, projectId, cloudVer) {
    this.request = (params) =>
      createRequest({
        ...params,
        service: 'guilds',
        credentials,
        projectId,
        cloudVer
      })
  }

  fetchGuilds ({param, value, cursor, limit, Direction}) {
    return this.request({
      endpoint: '[PROJECT_ID]/guild/query',
      query: {
        param,
        value,
        cursor,
        limit,
        Direction
      }
    })
  }

  searchGuilds (Name) {
    return this.request({
      endpoint: '[PROJECT_ID]/guild/search',
      query: {
        Name
      }
    })
  }

  getTopGuilds () {
    return this.request({
      endpoint: '[PROJECT_ID]/guild/search',
      query: {
        Fame: null,
        Direction: 'Down'
      }
    })
  }

  getGuildInfo (id) {
    return this.request({
      endpoint: '[PROJECT_ID]/guild/get',
      query: {
        id
      }
    })
  }
}
