import createRequest from '../createRequest'

export default function (requestParams) {
  let {
    service,
    cloudVer,
    projectId,
    credentials,
    ...params
  } = requestParams
  // Create final url
  let baseUrl = createBaseUrl(service, cloudVer)

  if (!projectId && params.endpoint.indexOf('[PROJECT_ID]' >= 0)) {
    return {err: 'Parameter "projectId" required but not specified.'}
  }

  params.endpoint = params.endpoint.replace('[PROJECT_ID]', projectId)

  // Get api-key
  const apiKey = credentials.services[service][projectId][cloudVer]

  if (!apiKey) {
    return {err: 'Api key not found.'}
  }

  params.headers = params.headers || {}
  params.headers['x-api-key'] = apiKey

  return createRequest(baseUrl, true)(params)
}

// INTERNAL METHODS

const createBaseUrl = (service, cloudVer) => {
  let url = `https://mfg-service-${service}`
  if (cloudVer === 'Development') {
    url += '-dev'
  }
  return url + '.appspot.com/'
}
