import createRequest from './core'

export default class PerformanceService {
  constructor (credentials, projectId, cloudVer) {
    this.request = (params) =>
      createRequest({
        ...params,
        service: 'performance',
        credentials,
        projectId,
        cloudVer
      })
  }

  fetchDevices () {
    return this.request({
      endpoint: '[PROJECT_ID]/admin/devices'
    })
  }

  fetchVersions () {
    return this.request({
      endpoint: '[PROJECT_ID]/admin/versions'
    })
  }

  fetchDeviceInfo (deviceId, version, grade) {
    return this.request({
      endpoint: '[PROJECT_ID]/admin/performances',
      query: {deviceId, version, grade}
    })
  }

  forceGrade (deviceId, grade, isValid, developer, version) {
    return this.request({
      endpoint: '[PROJECT_ID]/admin/customize',
      query: {deviceId, grade, setByAdmin: isValid, developer, version},
      method: 'put'
    })
  }
}
