import fetch from 'cross-fetch'
import qs from 'query-string'

const DEFAULT_HEADERS = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

export default (baseUrl, safe) => (requestParams) => {
  const {
    method = 'get',
    body,
    query,
    endpoint,
    headers = {},
    ...customOptions
  } = requestParams

  const options = {
    ...customOptions,
    headers: {...DEFAULT_HEADERS, ...headers}
  }
  let url = baseUrl + endpoint
  if (query) {
    url += `?${qs.stringify(query)}`
  }
  if (body) {
    if (options.headers['Content-Type'] === 'application/json' && typeof body === 'object') {
      options.body = JSON.stringify(body)
    } else {
      options.body = body
    }
  }
  const requestObject = {
    url,
    method,
    ...options
  }
  if (safe) {
    return internalRequestSafe(requestObject)
  }
  return internalRequest(requestObject)
}

// INTERNAL METHODS

// send fetch request OUTSIDE of a try/catch block
const internalRequest = async (params) => {
  const {url, ...options} = params
  const res = await fetch(url, options)
  return parseResponse(res)
}

// send fetch request INSIDE of a try/catch block
// "translate" request error into a more human readable message
const internalRequestSafe = async (params) => {
  const {url, ...options} = params
  let res
  try {
    res = await fetch(url, options)

    checkResponse(res, params)

    return parseResponse(res)
  } catch (err) {
    return {
      err: err.message || err,
      status: res && res.status
    }
  }
}

const checkResponse = (res, params) => {
  if (!res.ok) {
    let err = 'Something wrong happened.'

    if (res.status === 304) {
      err = 'Not modified.'
    }
    if (res.status === 401) {
      err = 'Request not authorized. (wrong x-api-key?)'
    }
    if (res.status === 404) {
      err = 'Targeted entity was not found on cloud.'
    }
    if (res.status === 405) {
      err = `Request endpoint not found for ${params.method.toUpperCase()} method.`
    }
    if (res.status === 409) {
      err = 'There was a conflict while processing the request.'
    }
    throw new Error(err)
  }
}

const parseResponse = (res) => {
  if (isJsonResponse(res)) {
    return jsonParseSafe(res)
  }
  return res.text()
}

const isJsonResponse = (res) =>
  res.headers.get('Content-Type').includes('application/json')

const jsonParseSafe = async (res) => {
  const data = await res.text()
  if (data) {
    return JSON.parse(data)
  }
  return data
}
