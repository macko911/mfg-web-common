import request from './core'

export default class AdminProducts {
  constructor (credentials, projectId, cloudVer, appVer) {
    this.request = (params) =>
      request({
        ...params,
        credentials,
        projectId,
        cloudVer,
        appVer
      })
  }

  async fetchUserInfo (uid, prop) {
    return (await this.fetchUserInfos(uid, prop))[uid]
  }

  async fetchUserInfos (uid, prop) {
    return this.request({
      endpoint: 'admin/profiles',
      query: {
        uid,
        prop
      }
    })
  }
}
