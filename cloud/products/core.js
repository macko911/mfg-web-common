import createRequest from '../createRequest'

const PRODUCTS = {
  'sg-legends': 'sgl'
}

export default function (requestParams) {
  let {
    projectId,
    cloudVer,
    appVer,
    credentials,
    ...params
  } = requestParams
  // fix discrepancy between projectId and product
  const product = PRODUCTS[projectId]
  if (!product) {
    return {err: `Product with projectId ${projectId} not found.`}
  }
  // Create final url
  let baseUrl = createBaseUrl(product, cloudVer, appVer)

  // Get api-key
  const apiKey = credentials.products[product][cloudVer]

  if (!apiKey) {
    return {err: 'Api key not found.'}
  }

  params.headers = params.headers || {}
  params.headers['x-api-key'] = apiKey

  return createRequest(baseUrl, true)(params)
}

// INTERNAL METHODS

const createBaseUrl = (product, cloudVer, appVer) => {
  let url = `https://mfg-product-${product}`

  // in dev mode, aim all requests to module "sgl"
  if (cloudVer === 'Development') {
    url = url + '-dev'
    url = url.replace('https://', 'https://sgl-dot-')
  }

  // in production mode, aim all requests to module
  // based on selected app version, f.e. appVer = 0.5.0 => module sgl-050
  if (cloudVer === 'Production') {
    url = url.replace('https://', `https://sgl-${appVer.replace(/\./g, '')}-dot-`)
  }

  // f.e. https://sgl-050-dot-mfg-product-sgl.appspot.com/
  // or   https://sgl-dot-mfg-product-sgl-dev.appspot.com/
  return url + '.appspot.com/'
}
