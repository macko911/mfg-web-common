import request from './core'

class CmsApi {
  fetch = (site, locale, section) =>
    request({
      endpoint: `cms/${site}/${locale}`,
      query: {section}
    })
}

export default new CmsApi()
