import request from './core'

export class TalentsApi {
  getTrending = (game) =>
    request({
      endpoint: `talents/trending/${game}`
    })

  getThumbnail = (videoId, size, width, height) =>
    request({
      endpoint: 'twitch/thumbnail',
      body: {
        id: videoId,
        size,
        width,
        height
      }
    })
}

export default new TalentsApi()
