import request from '../core'

export class UsersApi {
  fetchUserInfo = (id, projectId, cloudVer) =>
    request({
      endpoint: 'mfgcloud/users/profile',
      query: {
        id,
        projectId,
        cloudVer
      }
    })
}

export default new UsersApi()
