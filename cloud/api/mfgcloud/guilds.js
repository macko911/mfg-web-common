import request from '../core'

export class GuildsApi {
  fetchGuilds = ({param, value, cursor}, projectId, cloudVer, limit) =>
    request({
      endpoint: 'mfgcloud/guilds/query',
      query: {param, value, cursor, projectId, cloudVer, limit}
    })

  fetchTopGuilds = (projectId, cloudVer, limit) =>
    request({
      endpoint: 'mfgcloud/guilds/top',
      query: {projectId, cloudVer, limit}
    })

  fetchGuildInfo = (id, projectId, cloudVer) =>
    request({
      endpoint: 'mfgcloud/guilds/get',
      query: {id, projectId, cloudVer}
    })

  searchGuilds = (searchValue, projectId, cloudVer) =>
    request({
      endpoint: 'mfgcloud/guilds/search',
      query: {searchValue, projectId, cloudVer}
    })
}

export default new GuildsApi()
