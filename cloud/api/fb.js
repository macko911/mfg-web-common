import request from './core'

export class FacebookApi {
  fetchPosts = () =>
    request({
      endpoint: 'fb/posts'
    })
}

export default new FacebookApi()
