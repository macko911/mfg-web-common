import request from './core'

export class GetResponseApi {
  addSubscriber = (email, campaignId, name, customParams, notificationSettings) =>
    request({
      endpoint: 'gr/subscribe',
      body: {
        email,
        campaignId,
        name,
        customParams,
        notificationSettings
      },
      method: 'post'
    })

  addSubscriberCustom = (email, campaignId, customFields) =>
    request({
      endpoint: 'gr/subscribe-custom',
      body: {
        email,
        campaignId,
        customFields
      },
      method: 'post'
    })

  createPromoCodes = (email) =>
    request({
      endpoint: 'gr/promo-codes',
      method: 'post',
      query: {email}
    })

  getPromoCodes = (email) =>
    request({
      endpoint: 'gr/promo-codes',
      query: {email}
    })

  sendPromoCodes = (email) =>
    request({
      endpoint: 'gr/promo-codes/send',
      method: 'post',
      query: {email}
    })

  checkCustomFieldOption = (customFieldName, value) =>
    request({
      endpoint: `gr/custom-field/${customFieldName}/option`,
      body: {value},
      method: 'put'
    })
}

export default new GetResponseApi()
