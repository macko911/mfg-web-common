import request from './core'

export class LeaderboardsApi {
  range = ({prodId, arenaId, country = null, offset = 0, limit = 10}) =>
    request({
      endpoint: 'ldb/range',
      query: {
        prodId,
        arenaId,
        country,
        offset,
        limit
      }
    })

  ranks = ({prodId, arenaId, userIDs}) =>
    request({
      endpoint: 'ldb/ranks',
      query: {
        prodId,
        arenaId,
        userIDs
      }
    })

  userCount = ({prodId, arenaId, country}) =>
    request({
      endpoint: 'ldb/count',
      query: {
        prodId,
        arenaId,
        country
      }
    })
}

export default new LeaderboardsApi()
