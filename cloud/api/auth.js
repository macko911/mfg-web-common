import request from './core'

class AuthApi {
  login = (username, password) =>
    request({
      endpoint: 'auth',
      method: 'post',
      body: {
        username,
        password
      }
    })

  fbLogin = (id) =>
    request({
      endpoint: 'auth/facebook',
      method: 'post',
      body: {id}
    })
}

export default new AuthApi()
