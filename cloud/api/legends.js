import request from './core'

export class LegendsApi {
  getPower = () =>
    request({
      endpoint: 'legends/user/power'
    })
}

export default new LegendsApi()
