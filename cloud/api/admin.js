import request from './core'

class AdminApi {
  unlink = (request_token, fb_id) =>
    request({
      endpoint: 'admin/unlink',
      method: 'delete',
      query: {
        request_token,
        fb_id
      }
    })
}

export default new AdminApi()
