import request from './core'

export class LanguageApi {
  fetch = (site, locale, sections) =>
    request({
      endpoint: `lang/${site}/${locale}`,
      query: {
        sections
      }
    })
}

export default new LanguageApi()
