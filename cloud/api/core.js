import {API_URL} from '../../config'
import createRequest from '../createRequest'

export default createRequest(API_URL)
