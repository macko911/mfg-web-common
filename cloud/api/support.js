import request from './core'

export class SupportApi {
  didUserVote = (articleId) =>
    request({
      endpoint: `support/desk/article/${articleId}/vote`
    })

  voteArticle = (articleId, voteKind) =>
    request({
      endpoint: `support/desk/article/${articleId}/vote`,
      method: 'post',
      query: {kind: voteKind}
    })

  viewedArticle = (articleId, time) =>
    request({
      endpoint: `support/desk/article/${articleId}/view`,
      method: 'post',
      query: {time}
    })
}

export default new SupportApi()
