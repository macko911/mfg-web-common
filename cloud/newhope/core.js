import fetch from 'cross-fetch'
import _ from 'lodash'

export default async (requestParams) => {
  let {
    param,
    method = 'get',
    path,
    cloudVer = 'Real'
  } = requestParams

  if (!param) {
    return {
      err: 'missing required parameter "param"'
    }
  }
  if (!param.pw) {
    param.pw = process.env.NEW_HOPE_ADMIN_PW
  }
  let {cmd, prodId} = param
  let options

  let url = getFinalUrl(cmd, prodId, cloudVer)
  if (method === 'get') {
    url += '?param=' + encodeURIComponent(JSON.stringify(param))
  }
  if (method === 'post') {
    options = {
      body: 'param=' + JSON.stringify(param),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
  }
  const res = await internalRequest({
    url,
    method,
    ...options
  })
  // if path in response object specififed, try to get to the path
  if (path) {
    return _.get(res, path)
  }
  return res
}

// INTERNAL METHODS

const internalRequest = async (params) => {
  const {url, ...options} = params
  const res = await fetch(url, options)

  const data = await res.text()
  try {
    return JSON.parse(data)
  } catch (err) {
    return {
      err,
      response: data
    }
  }
}

const getBaseUrl = (cloudVer) => {
  switch (cloudVer.toLowerCase()) {
    case 'real':
    case 'production':
      return 'https://madfingerdatastore.appspot.com/'

    case 'developer':
    case 'development':
      return 'https://madfingergames-sgmp-dev.appspot.com/'
  }
}

const getModule = (prodId) => {
  switch (prodId) {
    case 'DeadTrigger2':
      return 'dt2/'

    case 'Unkilled':
      return 'uk/'

    case 'ShadowgunMP':
      return 'sgdz/'

    default:
      return 'web/'
  }
}

const getFinalUrl = (cmd, prodId, cloudVer) =>
  getBaseUrl(cloudVer) + getModule(prodId) + cmd
