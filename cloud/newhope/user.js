import request from './core'

const DEFAULT_PROPS = [
  'Username',
  'NickName',
  'Email',
  'AccountKind',
  'Ban',
  '_Friends',
  'IWantNews',
  'RegionInfo',
  'createDate',
  'RuntimePlatform'
]

export function getProductDataSid (sid, prodId) {
  return request({
    param: {
      cmd: 'playerSynchro',
      param: true,
      prodId,
      sid
    }
  })
}

export function getProductData (userid, pw, prodId) {
  return request({
    param: {
      cmd: 'getUsrPerProductDataList',
      param: ['Gold', 'Money', 'Rank', 'Version', '_Statistics'],
      prodId,
      userid,
      pw
    },
    path: ['data']
  })
}

export function getUserData (userid, pw, props = DEFAULT_PROPS) {
  return request({
    param: {
      cmd: 'getUsrDataList',
      userid,
      pw,
      param: props
    }
  })
}

// DEPRECATED: parsing response logic in cloud interface
export function parseUserDataResoponse (userid) {
  return (res) => {
    const init = {userid}
    return Object.keys(res).reduce((data, field) => {
      try {
        return {...data, [field]: JSON.parse(res[field])}
      } catch (err) {
        return {...data, [field]: res[field]}
      }
    }, init)
  }
}

export function applyVoucher (userid, pw, appVer, prodId, code) {
  return request({
    param: {
      cmd: 'voucherApply',
      param: code,
      userid,
      pw,
      appVer,
      prodId
    },
    method: 'post'
  })
}

export function recoverAccount (email) {
  return request({
    param: {
      cmd: 'requestResetAllAssociatedPasswords',
      email
    },
    method: 'post'
  })
}

export function fbJoinSession (token, prodId) {
  return request({
    param: {
      cmd: 'userJoinSession',
      prodId,
      provider: 'Facebook',
      pw: token
    },
    method: 'post'
  })
}

export function getUserFromFacebookId (facebookId) {
  return request({
    param: {
      cmd: 'queryUsersByField',
      param: 'FacebookUnkilled',
      param2: facebookId,
      val: 100,
      data: true
    },
    path: [0, 'id']
  })
}

// Possible proviers: Google, FacebookUnkilled, GameCenter
export function unlinkAccount (userid, provider) {
  return request({
    param: {
      cmd: 'unlinkAccount',
      userid,
      // force uk module as that is the only one that uses unlinking accounts
      prodId: 'Unkilled',
      param: provider
    },
    method: 'post'
  })
}

export function getPrimaryKey (userid, cloudVer) {
  return request({
    param: {
      cmd: 'userGetPrimaryKey',
      userid
    },
    cloudVer,
    path: ['data', 'primaryKey']
  })
}

export function fbGetUserId (facebookId) {
  return request({
    param: {
      cmd: 'getLinkedUsers',
      param: 'FacebookID',
      param2: 'Any',
      data: facebookId
    },
    path: ['result', 'userid']
  })
}

export function setUserDataProp (userid, property, value) {
  return request({
    param: {
      cmd: 'setUsrData',
      userid,
      param: property,
      data: value
    },
    method: 'post'
  })
}

export function setUserData (userid, changes, prodId, cloudVer, secretCode) {
  return request({
    param: {
      cmd: 'setUsrDataList',
      userid,
      prodId,
      param: changes,
      sc: secretCode
    },
    method: 'post',
    cloudVer
  })
}
