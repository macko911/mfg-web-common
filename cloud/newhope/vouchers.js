import moment from 'moment'
import request from './core'

class VouchersNewHope {
  constructor (cloudVer) {
    this.request = (params) => request({...params, cloudVer})
  }
  // exmple data:
  // {
  //   Author: 'mvobornik',
  //   Description: 'Hello world.',
  //   GeneratedAt: '2018-01-01 00:00:00',
  //   GeneratedCount: '1',
  //   MaxPerUserCount: '1',
  //   MaxVoucherUsageCount: '1',
  //   MinimalAppVersion: '',
  //   Product: 'Unkilled',
  //   Rewards: [],
  //   ValidFrom: '2018-01-01 00:00:00',
  //   ValidUntil: '2018-01-01 00:00:00'
  // }
  voucherSetCreate (name, data) {
    return this.request({
      method: 'post',
      param: {
        cmd: 'voucherSetCreate',
        param: {
          ...data,
          GeneratedAt: moment().format('YYYY-MM-DD HH:mm:ss')
        },
        param2: name,
        prodId: data.Product
      }
    })
  }

  voucherSetEdit (name, data) {
    return this.request({
      method: 'post',
      param: {
        cmd: 'voucherSetUpdate',
        param: {
          ...data,
          GeneratedAt: moment().format('YYYY-MM-DD HH:mm:ss')
        },
        param2: name,
        prodId: data.Product
      }
    })
  }

  getVoucherSetInfo (name, prodId) {
    return this.request({
      param: {
        cmd: 'voucherSetGet',
        param: name,
        prodId
      },
      path: 'data.voucherSet'
    })
  }

  getVoucherSetCodes (name, prodId) {
    return this.request({
      param: {
        cmd: 'voucherGetCodes',
        param: name,
        prodId
      },
      path: 'data.usageCounts'
    })
  }
}

export default VouchersNewHope
