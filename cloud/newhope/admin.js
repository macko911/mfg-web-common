import request from './core'

export const addAdminDevIp = (ip, description) =>
  request({
    param: {
      cmd: 'adminDevIPAdd',
      param: ip,
      param2: description
    },
    cloudVer: 'development',
    method: 'post'
  })

export const getGlobalSettings = (prodId, cloudVer) =>
  request({
    param: {
      cmd: 'globalDataDesc',
      prodId
    },
    method: 'post',
    cloudVer
  })
