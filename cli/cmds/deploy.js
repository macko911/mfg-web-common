import chalk from 'chalk'
import findUp from 'find-up'
import {
  exec,
  getService,
  getDevUsername,
  getGitCommitHash,
  resolveProject,
  clearConsole
} from '../utils/misc'
import {getConfig, editConfig, createDeployConfig} from '../utils/config'
import {notifyWebTeam} from '../../utils/slack'
import q from '../utils/questions'

async function deploy (argv) {
  clearConsole()
  let options = [
    `--project ${argv.project || 'madfingergames-web'}`,
    '--quiet',
    '--verbosity=warning'
  ]
  // prepare environment
  const service = getService()

  console.log('🐿  ' + chalk.bgYellow.black(` MADFINGER Games deployment - ${service}! `) + '  🐿')

  const {version, isDefault, deployApi} = await setup(options, argv)

  const configPath = argv.config ? resolveProject(argv.config) : createDeployConfig(service)

  // deploy api service inception way!
  if (deployApi) {
    exec(`cd ${findUp.sync('api')} && yarn deploy --quiet --ver ${version} ${isDefault ? '--default' : ''}`)
  }

  // notify web team
  // await notify(service, isDefault, version)

  // deploy service!
  exec(`gcloud app deploy ${configPath} ${options.join(' ')}`)
}

async function setup (options, argv) {
  let {version, withGitHash, isDefault, deployApi} = await ask(argv)
  if (!version) {
    throw new Error(chalk.yellow('Missing required parameter --ver. Quitting deployment!'))
  }
  // edit version name
  version = version.replace(/\s/g, '-')
  editConfig('version', version)
  process.env.APP_VERSION = version
  if (withGitHash) {
    version = `${getGitCommitHash()}-${version}`
  }
  options.push(`--version ${version}`)

  if (isDefault) {
    options.push('--promote')
    options.push('--stop-previous-version')
  } else {
    options.push('--no-promote')
  }

  return {
    version,
    isDefault,
    deployApi,
    options
  }
}

async function ask (argv) {
  if (argv.quiet) {
    return {
      version: argv['ver'], // required
      withGitHash: argv['git-hash'] || false,
      isDefault: argv['default'] || false
    }
  } else {
    const questions = q.init(getConfig())
    const picked = ['version', 'withGitHash', 'isDefault']
    if (getService() !== 'api') {
      picked.push('deployApi')
    }
    return q.ask(questions, picked)
  }
}

async function notify (service, isDefault, version) {
  const developer = await getDevUsername()

  console.log(chalk.gray('\nNotifying web team...\n'))

  await notifyWebTeam(
    `:tada: User *@${developer}* started deploying to service *${service}* as ${
      isDefault ? '_default_ ' : ''
    }version ${version}...`
  )
}

export default {
  command: ['deploy [args]', 'publish', 'shipit'],
  desc: 'Deploy current application to cloud.',
  builder: (yargs) => yargs
    .option('v', {
      alias: 'ver',
      describe: 'name of deployed version',
      type: 'string',
      nargs: 1
    })
    .option('q', {
      alias: 'quiet',
      describe: "don't ask me any questions",
      type: 'boolean',
      nargs: 0
    })
    .option('p', {
      alias: 'project',
      describe: 'gcloud project to deploy',
      type: 'string',
      default: 'madfingergames-web',
      nargs: 0
    })
    .option('config', {
      describe: 'different path to app.yaml than the auto-generated one',
      type: 'string',
      nargs: 0
    })
    .option('git-hash', {
      describe: 'prepend current git commit hash to version name',
      type: 'boolean',
      nargs: 0
    })
    .example('mfg deploy --ver test --quiet'),
  handler: deploy
}
