import concurrently from 'concurrently'
import {resolveCli} from '../utils/misc'

// NOTE: sub-commands are relative to folder from which this command is called
const COMMANDS = [
  'gatsby develop', // local react dev server
  `node ${resolveCli('./')} cdn`, // local CDN file server
  'cd ../../api && yarn dev'
]

function dev () {
  concurrently(COMMANDS)
}

export default {
  command: 'dev',
  desc: 'Start a client-side development server',
  builder: () => {},
  handler: dev
}
