import express from 'express'
import cors from 'cors'
import {resolveCli} from '../utils/misc'

async function cdn () {
  const app = express()

  // TODO: ask for custom path to local CDN repository on first run
  // expects cloud-storage git repo to be in the same folder as current repo
  const pathToCdn = resolveCli('../../cloud-storage/mfg-home/')

  app.use(cors())

  app.use(express.static(pathToCdn))

  // handle urls not found in cloud storage
  app.get('*', function (req, res) {
    res.status(404).send('Static file not found.')
  })

  app.listen(9999, () => {
    console.log('CDN server listening @ http://localhost:9999')
  })
}

export default {
  command: 'cdn',
  desc: 'Start a development server with static files',
  builder: () => {},
  handler: cdn
}
