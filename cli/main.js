import yargs from 'yargs'
import chalk from 'chalk'
import cdn from './cmds/cdn'
import dev from './cmds/dev'
import deploy from './cmds/deploy'

export default async function () {
  try {
    yargs
      .usage('mfg <cmd> [options]')
      .command(cdn)
      .command(dev)
      .command(deploy)
      .epilog(chalk.yellow('😈 See you next time!'))
      .help('help', 'show this help screen')
      // don't show --version option
      .option('version', {
        hidden: true
      })
      // wrap output to 100 chars or terminal width
      .wrap(Math.min(100, yargs.terminalWidth()))
      // show help if no command specified
      .demandCommand(1, '')
      .argv
  } catch (err) {
    console.log(err)
  }
}
