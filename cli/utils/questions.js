import inquirer from 'inquirer'
import chalk from 'chalk'

const ask = (questions, names) => inquirer.prompt(names.map(name => questions[name]))

const init = (prev = {}) => {
  const version = {
    name: 'version',
    type: 'input',
    message: 'What is the name of the new version?',
    default: prev.version || 'noversion'
  }

  const withGitHash = {
    name: 'withGitHash',
    type: 'confirm',
    message: 'Add git hash to version?',
    default: true
  }

  const deployApi = {
    name: 'deployApi',
    type: 'confirm',
    message: 'Also deploy Node API server?',
    default: false
  }

  const isDefault = {
    name: 'isDefault',
    type: 'confirm',
    message: 'Deploy as default version?',
    default: false
  }

  return {
    version,
    withGitHash,
    deployApi,
    isDefault
  }
}

export const promptContinue = async (defaultResult) => {
  const question = {
    type: 'confirm',
    name: 'response',
    message: chalk.yellow('Continue'),
    default: defaultResult
  }
  return (await ask([question])).response
}

export default {
  init,
  ask,
  promptContinue
}
