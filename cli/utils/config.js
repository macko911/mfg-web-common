import chalk from 'chalk'
import fs from 'fs'
import yaml from 'yamljs'

import {resolveProject, resolveCli} from './misc'

const configPath = resolveProject('mfg.config.json')

const SERVICES = {
  api: {
    instance_class: 'F4_1G'
  },
  unkilled: {
    instance_class: 'F1'
  },
  monzo: {
    instance_class: 'F1'
  },
  monzovr: {
    instance_class: 'F1'
  }
}

const IC_MEMORY = {
  F4_1G: 1024,
  F4: 512,
  F2: 256,
  F1: 128
}

const getServiceMemory = (service) => {
  const {instance_class} = SERVICES[service]
  return IC_MEMORY[instance_class]
}

export const getConfig = () => {
  try {
    return require(configPath)
  } catch (err) {
    console.log(chalk.gray('mfg.config.json not found or corrupted, creating new file...'))
    saveConfig({})
    return {}
  }
}

export const editConfig = (prop, value) => {
  const config = getConfig()

  config[prop] = value
  saveConfig(config)
}

const saveConfig = (data) =>
  fs.writeFileSync(configPath, JSON.stringify(data, null, 2))

/**
 * create app.yaml with deploy info in the current project directory
   @param {string} service service to deploy
 * @returns {string} path to created config file
 */
export const createDeployConfig = (service) => {
  const yamlConfig = yaml.load(resolveCli('../app.yaml'))
  const configPath = resolveProject('./app.deploy.yaml')
  // override default config
  const newConfig = {
    service,
    ...SERVICES[service]
  }
  const configData = yaml.stringify({...yamlConfig, ...newConfig})
  fs.writeFileSync(configPath, configData)
  // set available RAM for runninng instance
  // NOTE: this is here to lower memory alocation for app in production
  editConfig('memory', getServiceMemory(service))
  return configPath
}

export default {
  getConfig,
  editConfig,
  createDeployConfig
}
