import chalk from 'chalk'
import path from 'path'
import fetch from 'cross-fetch'
import {execSync} from 'child_process'

const SERVICES = ['monzo', 'monzovr', 'unkilled', 'api', 'admin']

/**
 * @typedef {Object} AppConfig
 * @property {string} service name of service
 */

/**
 * Resolve absolute path of file relative to current directory
 * @param {string} p relative path to file
 * @returns {string} path absolute path to file
 */
export const resolveCli = (p) => path.resolve(__dirname, '../', p)

/**
 * Resolve absolute path of file relative to current project from which cli is used
 * @param {string} p relative path to file
 * @returns {string} absolute path to file
 */
export const resolveProject = (p) => path.resolve(process.cwd(), p)

/**
 * Runs a synchronous child_process
 * @param {string} cmd command to run
 * @returns {void}
 */
export const exec = (cmd) => execSync(cmd, {stdio: 'inherit'})

/**
 * Returns name of the website (aka service) currently operated on
 * @returns {string} website
 */
export const getService = () => {
  const service = path.basename(process.cwd())
  if (!SERVICES.includes(service)) {
    throw new Error(chalk.yellow(`Service ${service} not supported!`))
  }
  return service
}

const WEBHOOK_URL = 'https://chat.googleapis.com/v1/spaces/AAAAeLdIjCc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=N6MwV0XtigXzhRLLUTiL_5Dla3gfk1zGnze0se2rs9I%3D'

/**
 * Sends a message to Google Chat Web channel
 * @param {string} text message to send
 * @returns {void}
 */
export const notifyWebTeam = (text) => fetch({
  url: WEBHOOK_URL,
  body: JSON.stringify({text}),
  method: 'post'
})

/**
 * Returns current user that is logged into gcloud utils
 * @returns {string} username, f.e. mvobornik or jkamen
 */
export const getDevUsername = () => {
  try {
    const res = execSync('gcloud config list account --format "value(core.account)"').toString()
    const username = res.trim()
    return username && username.replace('@madfingergames.com', '') || 'unknown'
  } catch (err) {
    console.log(err)
    return 'unknown'
  }
}

/**
 * Returns current git commit hash
 * @returns {string} hash
 */
export const getGitCommitHash = () => {
  return execSync('git rev-parse --short HEAD').toString().trim()
}

/**
 * Returns current git branch
 * @returns {string} branch
 */
export const getGitBranch = () => {
  return execSync('git rev-parse --abbrev-ref HEAD').toString().trim()
}

/**
 * Clears termianl screen
 * @returns {void}
 */
export const clearConsole = () => {
  process.stdout.write(
    process.platform === 'win32' ? '\x1B[2J\x1B[0f' : '\x1B[2J\x1B[3J\x1B[H'
  )
}
