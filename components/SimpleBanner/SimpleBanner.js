import React from 'react'
import PropTypes from 'prop-types'

import Link from '../Link'
import css from './SimpleBanner.module.scss'

const SimpleBanner = ({link, imageSrc, utmSource, className}) => {
  return (
    <Link
      to={link}
      campaign={`utm_source=${utmSource}&utm_medium=banner`}
      className={`${className} ${css.bannerWrap}`}
    >
      <div
        className={css.banner}
        style={{backgroundImage: `url(${imageSrc})`}}
      />
    </Link>
  )
}

/*
   link: url to open on click
   imageSrc: image source of banner image :)
   utmSource: source website
   className: additional css class for wrapper if necessary
*/

SimpleBanner.propTypes = {
  link: PropTypes.string.isRequired,
  imageSrc: PropTypes.string.isRequired,
  utmSource: PropTypes.string,
  className: PropTypes.string
}

SimpleBanner.defaultProps = {
  className: '',
  utmSource: 'monzo' // TODO: put into common module config
}

export default SimpleBanner
