import React from 'react'
import PropTypes from 'prop-types'

import SimpleBanner from './SimpleBanner'
import {CDN_ROOT} from '../config'

const WargamesBanner = ({utmSource, site}) => (
  <SimpleBanner
    link='https://www.shadowgunwargames.com'
    imageSrc={`${CDN_ROOT}wargames/images/web_banner${site ? '_' + site : ''}.jpg`}
    utmSource={utmSource}
    className='release-banner'
  />
)

WargamesBanner.propTypes = {
  utmSource: PropTypes.string,
  site: PropTypes.string
}

export default WargamesBanner
