import React from 'react'
import PropTypes from 'prop-types'

import {
  FacebookShareButton,
  FacebookIcon,
  GooglePlusShareButton,
  GooglePlusIcon,
  TwitterShareButton,
  TwitterIcon
} from 'react-share'

const SocialShare = (props) => {
  let {url, title, size = 36} = props

  if (!url) {
    url = location && location.href
  }
  return (
    <div className='social-share-wrap flex'>
      <FacebookShareButton
        style={styles.btn}
        url={url}
        quote={title}
      >
        <FacebookIcon size={size} logoFillColor='white' />
      </FacebookShareButton>

      <TwitterShareButton
        style={styles.btn}
        url={url}
        title={title}
      >
        <TwitterIcon size={size} logoFillColor='white' />
      </TwitterShareButton>

      <GooglePlusShareButton
        style={styles.btn}
        url={url}
      >
        <GooglePlusIcon size={size} logoFillColor='white' />
      </GooglePlusShareButton>
    </div>
  )
}

SocialShare.propTypes = {
  url: PropTypes.string,
  size: PropTypes.number,
  title: PropTypes.string
}

export default SocialShare

const styles = {
  btn: {
    cursor: 'pointer'
  }
}
