import React from 'react'
import storage from '../../utils/storage'

import css from './CookiesBanner.module.scss'

class CookiesBanner extends React.Component {
  state = {
    showCookiesWarning: false
  }

  componentDidMount () {
    if (!storage.get('cookies-warning-shown')) {
      this.setState({showCookiesWarning: true})
    }
  }

  markCookiesShown = () => {
    storage.set('cookies-warning-shown', 'true')
    this.setState({showCookiesWarning: false})
  }

  render () {
    const {showCookiesWarning} = this.state

    if (!showCookiesWarning) {
      return null
    }
    return (
      <div className={css.wrap}>
        <div className={css.text}>
          <span>
            {'We are using cookies.'}
          </span>
        </div>

        <div className={css.button} onClick={this.markCookiesShown}>
          <span>{'OK'}</span>
        </div>
      </div>
    )
  }
}

export default CookiesBanner
