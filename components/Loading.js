import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import css from './Loading.module.scss'

const Loading = ({small, light, dark, cube, style = {}, noMargin}) => {
  if (cube) {
    return <div className={cn(css.loading, css.foldingCube)} style={style}>
      <div className={cn(css.cube, css.cube1)} />
      <div className={cn(css.cube, css.cube2)} />
      <div className={cn(css.cube, css.cube4)} />
      <div className={cn(css.cube, css.cube3)} />
    </div>
  }
  return (
    <div
      className={cn(
        css.loading,
        css.spinner,
        small && css.small,
        light && css.light,
        dark && css.dark,
        noMargin && css.noMargin
      )}
      style={style}
    >
      <div className={css.bounce1} />
      <div className={css.bounce2} />
      <div className={css.bounce3} />
    </div>
  )
}

Loading.propTypes = {
  small: PropTypes.bool,
  light: PropTypes.bool,
  dark: PropTypes.bool,
  style: PropTypes.object,
  cube: PropTypes.bool,
  noMargin: PropTypes.bool
}

export default Loading
