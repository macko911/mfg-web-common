export default {
  // MADFINGER
  'mfg-logo-full': 'https://www.madfingergames.com',
  'mfg-logo-head': 'https://www.madfingergames.com',

  // WEBSITES
  'uk-web': 'http://www.unkilledgame.com',
  'dt2-web': 'https://www.madfingergames.com/deadtrigger2',
  'sgdz-web': 'https://www.madfingergames.com/deadzone',
  'monzo-web': 'http://www.monzoapp.com',
  'legends-web': 'http://www.shadowgunlegends.com/',

  // STORE LINKS
  'monzovr-oculus': 'http://ocul.us/2lg3kqN',
  'monzovr-gearvr': 'http://ocul.us/2k6xxXt',

  'uk-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.unkilled',
  'uk-appstore': 'https://itunes.apple.com/app/apple-store/id969488951',
  'unkilled-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.unkilled',
  'unkilled-appstore': 'https://itunes.apple.com/app/apple-store/id969488951',

  'dt2-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.deadtrigger2',
  'dt2-appstore': 'https://itunes.apple.com/app/apple-store/id720063540',
  'dt2-facebook': 'https://apps.facebook.com/deadtrigger_ii/',
  'dt2-amazon': 'http://www.amazon.com/MADFINGER-Games-a-s-DEAD-TRIGGER/dp/B00J06HWC4',
  'dt2-windows': 'https://www.microsoft.com/en-us/store/p/dead-trigger-2/9nblgggzm624',

  'monzo-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.monzo',
  'monzo-appstore': 'https://itunes.apple.com/app/apple-store/id906362026',

  'legends-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.legends',
  'legends-appstore': 'https://itunes.apple.com/app/apple-store/id1091251242',

  'sgdz-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.deadzone',
  'sgdz-appstore': 'https://itunes.apple.com/app/apple-store/id561048152',
  'sgdz-facebook': 'https://apps.facebook.com/shadowgun-deadzone/',

  'dt-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.deadtrigger',
  'dt-appstore': 'https://itunes.apple.com/app/apple-store/id533079551',

  'sg-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.shadowgun',
  'sg-appstore': 'https://itunes.apple.com/app/apple-store/id440141669',
  'sg-amazon': 'http://www.amazon.com/MADFINGER-Games-a-s-SHADOWGUN/dp/B007TM2J1A',
  'shadowgun-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.shadowgun',
  'shadowgun-appstore': 'https://itunes.apple.com/app/apple-store/id440141669',
  'shadowgun-amazon': 'http://www.amazon.com/MADFINGER-Games-a-s-SHADOWGUN/dp/B007TM2J1A',

  'samurai2-gplay': 'https://play.google.com/store/apps/details?id=com.madfingergames.SamuraiIIAll',
  'samurai2-appstore': 'https://itunes.apple.com/app/apple-store/id392486160',
  'samurai2-mac': 'https://itunes.apple.com/app/apple-store/id423743523',
  'samurai2-amazon': 'https://www.amazon.com/MADFINGER-Games-a-s-Samurai-Vengeance/dp/B004Y218RO',

  'samurai-appstore': 'https://itunes.apple.com/app/apple-store/id328219302',

  'fb_dt2': 'https://www.facebook.com/DEADTRIGGER',
  'fb_sg': 'https://www.facebook.com/Shadowgun',
  'fb_mfg': 'https://www.facebook.com/madfingergames/',
  'gplus': 'https://plus.google.com/111347781406998500019/posts',
  'twitter': 'https://twitter.com/MADFINGERGames',
  'forum': 'http://forum.madfingergames.com/forum',
  'youtube': 'https://www.youtube.com/user/MadFingerGames'
}
