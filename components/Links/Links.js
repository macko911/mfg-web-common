import React from 'react'
import PropTypes from 'prop-types'
// import Analytics from 'ext/google-analytics'

import urls from './urls'

const rename = name => name.replace('-', ' ')

const Links = ({
  name,
  className = '',
  campaign = '',
  ...props
}) => {
  let link = urls[name]

  const onClick = (e, ...rest) => {
    // Analytics.click(link, 'Outbound Link')
    if (props.onClick) {
      props.onClick(e, ...rest)
    }
  }

  // add campaign parameters to link
  if (campaign) {
    // check if there's a query already specified in the "to" field
    link += (link.indexOf('?') < 0 ? '?' : '&') + campaign
  }

  return (
    <a href={link} target='_blank'>
      <div {...props}
        onClick={onClick}
        className={`shared links ${rename(name)} ${className}`} />
    </a>
  )
}

Links.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
  campaign: PropTypes.string
}

export default Links
