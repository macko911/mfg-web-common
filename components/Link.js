import React from 'react'
import PropTypes from 'prop-types'
import {Link as GatsbyLink} from 'gatsby'
import analytics from '../utils/analytics'

const Link = ({to = '', campaign, sameWindow, ...props}) => {
  let link = to

  const onClick = (...params) => {
    analytics.click(link, 'Outbound Link')
    if (props.onClick) {
      props.onClick(...params)
    }
  }

  // add campaign parameters to link
  if (campaign) {
    // check if there's a query already specified in the "to" field
    link += (link.indexOf('?') < 0 ? '?' : '&') + campaign
  }

  // if this is an external link
  if (to.indexOf('http') === 0 || props.deepLink) {
    return (
      <a
        href={link}
        className={props.className}
        onClick={onClick}
        target={sameWindow ? '_self' : '_blank'}>
        {props.children}
      </a>
    )
  }

  return <GatsbyLink to={link} {...props} onClick={onClick} />
}

Link.propTypes = {
  to: PropTypes.string,
  children: PropTypes.any,
  className: PropTypes.string,
  deepLink: PropTypes.bool,
  sameWindow: PropTypes.bool,
  campaign: PropTypes.string,
  onClick: PropTypes.func
}

export default Link
