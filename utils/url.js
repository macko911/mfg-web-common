// remove leading / in the path
export const removeLeadingSlash = (path) => {
  if (path.charAt(0) === '/') {
    return path.slice(1)
  }
  return path
}

// remove trailing / in the path
export const removeTrailingSlash = (path) => {
  if (path.charAt(path.length - 1) === '/') {
    return path.slice(0, path.length - 1)
  }
  return path
}

// add / at the end of path
export const addTrailingSlash = (path) => {
  if (path.charAt(path.length - 1) !== '/') {
    path += '/'
  }
  return path
}

// removes duplicate slashes at the beginning or end of path url
export const resolvePath = (...params) =>
  params.reduce((url, part) => addTrailingSlash(url) + removeLeadingSlash(part), '')

// replace all non-alphanumeric characters with spaces
export const makeUrl = (string) => string.toLowerCase().replace(/\W+/g, '-')
