import fetch from 'cross-fetch'

// PUBLIC METHODS

export const notifyWebTeam = (message) => {
  return fetch(WEBMONITORING_CHANNEL, {
    method: 'post',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({text: message})
  })
}

// WEBHOOK URLS

const TEST_CHANNEL = 'https://hooks.slack.com/services/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'
const WEB_TEAM_CHANNEL = 'https://hooks.slack.com/services/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'
const WEBMONITORING_CHANNEL = 'https://hooks.slack.com/services/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'
