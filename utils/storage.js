// clear expired storage items
const clearExpired = () => {
  Object.keys(localStorage).forEach(key => {
    let item = localStorage.getItem(key)
    try {
      const {maxAge, date} = JSON.parse(item)
      const age = (new Date() - new Date(date)) / 1000 / 60 / 60 / 24 // in days
      if (maxAge && (age > maxAge)) {
        localStorage.removeItem(key)
      }
    } catch (err) {}
  })
}

class LocalStorage {
  constructor () {
    if (typeof localStorage === 'undefined') return
    clearExpired()
  }

  // maxAge is optional and is counted in Days
  set = (key, value, maxAge = 365) => {
    if (typeof localStorage === 'undefined') return
    let valueSafe = value
    if (typeof value === 'object') {
      valueSafe = JSON.stringify(value)
    }
    const item = JSON.stringify({
      value: valueSafe,
      date: new Date().toISOString(),
      maxAge
    })
    localStorage.setItem(key, item)
    return value
  }

  get = (key) => {
    if (typeof localStorage === 'undefined') return
    let item = localStorage.getItem(key)
    if (item) {
      try {
        item = JSON.parse(item)
        if (item.value) {
          try {
            return JSON.parse(item.value)
          } catch (err) {
            return item.value
          }
        }
      } catch (err) {}
      return item
    }
  }

  remove = (key) => {
    if (typeof localStorage === 'undefined') return
    localStorage.removeItem(key)
  }
}

export default new LocalStorage()
