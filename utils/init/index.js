import analytics from '../analytics'
import fonts from './fonts'

export default function (options) {
  const {site} = options

  fonts()
  analytics.init(site)
}
