import {library} from '@fortawesome/fontawesome-svg-core'
import {
  faBars,
  faTimes,
  faCheck,
  faVolumeOff,
  faAngleDown,
  faAngleRight,
  faAngleLeft,
  faTicketAlt
} from '@fortawesome/free-solid-svg-icons'
import {
  faFacebookSquare,
  faTwitter,
  faYoutube,
  faTwitch
} from '@fortawesome/free-brands-svg-icons'

export default function () {
  library.add(
    // solid
    faBars,
    faTimes,
    faCheck,
    faVolumeOff,
    faAngleDown,
    faAngleRight,
    faAngleLeft,
    faTicketAlt,
    // brands
    faFacebookSquare,
    faTwitter,
    faYoutube,
    faTwitch
  )
}
