// platform detection
export const isApple = (info) => ['OS X', 'iOS'].indexOf(info.os.family) >= 0
export const isAndroid = (info) => ['Android', 'Linux'].indexOf(info.os.family) >= 0
export const isMobile = (info) => ['Android', 'iOS'].indexOf(info.os.family) >= 0
