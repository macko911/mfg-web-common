import React, {Component} from 'react'
import {css, cx} from 'emotion'

import raven from '../raven'
import {getDisplayName} from './utils'

const catchErrors = (WrappedComponent) => {
  class ErrorBoundary extends Component {
    state = {
      error: null,
      errorInfo: null
    }

    componentDidCatch = (error, errorInfo) => {
      this.setState({error, errorInfo})
      // send error to Sentry
      raven.captureException(error, {extra: errorInfo})
    }

    render () {
      const {error, errorInfo} = this.state

      if (!errorInfo) {
        return <WrappedComponent {...this.props} />
      }
      return (
        <div className={cx(styles.wrap, 'flex column animated fadeIn')}>
          <div className={styles.header}>
            <div className='container flex align-right'>
              <a href='https://www.madfingergames.com/'>
                <div className='shared mfg-logo small' />
              </a>
            </div>
          </div>

          <div className='container flex column hcenter'>
            <h2>{'Something went wrong.'}</h2>
            <p className={styles.text}>
              {"Don't worry, our team has been notified. You can either contact us via our "}
              <a href='https://www.madfingergames.com/contact'>{'contact form'}</a>
              {' or '}
              <a href={location.href}>{'refresh the page'}</a>
              {' and try again.'}
            </p>

            {process.env.NODE_ENV === 'development' &&
              <details className={styles.stack}>
                <div style={{whiteSpace: 'pre-wrap'}}>
                  {error && error.toString()}
                  <br />
                  {errorInfo.componentStack}
                </div>
              </details>
            }
          </div>
        </div>
      )
    }
  }
  ErrorBoundary.displayName = `ErrorBoundary(${getDisplayName(WrappedComponent)})`
  return ErrorBoundary
}

const styles = {
  wrap: css`
    background: lighten(black, 2%);
    color: white;
    min-height: 100vh;

    .container {
      padding: 20px 10px;
      flex: 1;
    }
  `,
  text: css`
    color: darken(white, 40%);
    a {
      color: darken(white, 10%);
      font-weight: bold;

      &:hover {
        color: white;
      }
    }
  `,
  header: css`
    width: 100%;
  `,
  stack: css`
    width: 100%;
    padding: 20px;
    border: 1px solid lighten(black, 15%);
    background: lighten(black, 5%);
    border-radius: 3px;
  `
}

export default catchErrors
