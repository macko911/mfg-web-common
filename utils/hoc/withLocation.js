import React from 'react'
import {Location} from '@reach/router'

export default (Component) => {
  class WithLocation extends React.Component {
    render () {
      return (
        <Location>
          {({location}) => <Component {...this.props} location={location} />}
        </Location>
      )
    }
  }
  return WithLocation
}
