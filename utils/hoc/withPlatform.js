import React from 'react'
import hoistNonReactStatic from 'hoist-non-react-statics'
import platform from 'platform'

import {getDisplayName} from './utils'

export default (WrappedComponent) => {
  class EnhancedComponent extends React.Component {
    state = {
      platform: null
    }

    componentDidMount () {
      this.setState({platform})
    }

    render () {
      return (
        <WrappedComponent {...this.props} {...this.state} />
      )
    }
  }
  EnhancedComponent.displayName = `WithPlatform(${getDisplayName(EnhancedComponent)})`
  return hoistNonReactStatic(EnhancedComponent, WrappedComponent)
}
