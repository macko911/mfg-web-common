import platform from 'platform'

export const smoothScroll = (id, timeout) => {
  // get DOM element to scroll to
  const elem = typeof id === 'string' ? document.querySelector(id) : id
  if (!elem) {
    console.log(`SmoothScroll error: Elem ${id} not found.`)
  }
  // create method for scrolling
  const scrollIntoView = () => {
    if (!elem) {
      return
    }
    if (platform.name === 'IE' || platform.name === 'Microsoft Edge') {
      elem.scrollIntoView()
    } else {
      elem.scrollIntoView({behavior: 'smooth', block: 'start'})
    }
  }
  if (timeout) {
    setTimeout(scrollIntoView, timeout)
    return
  }
  scrollIntoView()
}

export const preloadImage = (url) => {
  new Image().src = url
}
