import Raven from 'raven-js'

const isDev = process.env.NODE_ENV === 'development'

const RAVEN_URL = 'https://44d30735d90b4a059a7e611a91154540@sentry.io/97592'

let RavenQueue = {}

// after logging an error, remove it from the queue
if (typeof document !== 'undefined') {
  document.addEventListener('ravenSuccess', (e) => {
    delete RavenQueue[e.data.culprit]
  })
}

const ravenConfig = Raven.config(RAVEN_URL, {
  environment: 'production',
  // TODO: add release name
  release: 'mfg-web-common',
  shouldSendCallback: (data) => {
    // don't send data to server in development mode,
    // or if sent error is already in queue
    if (isDev || data.culprit in RavenQueue) {
      return false
    }
    RavenQueue[data.culprit] = true
    return true
  }
})

if (!isDev) {
  ravenConfig.install()
}

export default Raven
