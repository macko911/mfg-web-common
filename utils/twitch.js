import fetch from 'cross-fetch'

const CLIENT_ID = 'gm0k6xilx0zc5wwbz3csvbnowtyl1i'
const BASE_URL = 'https://api.twitch.tv/kraken/'
const MFG_USER_ID = '77388395'

// * ******** PRIVATE METHODS ********* *

const request = (endpoint) =>
  fetch(`${BASE_URL}${endpoint}`, {
    headers: {
      Accept: 'application/vnd.twitchtv.v5+json',
      'Client-ID': CLIENT_ID
    }
  }).then(res => res.json())

// * ******** PUBLIC METHODS ********* *

export const fetchVideos = async (channelId = MFG_USER_ID) =>
  request(`channels/${channelId}/videos`)

export const fetchStreamsByGame = async (game) =>
  request(`streams?game=${game}`)

export const fetchStreamsByChannelId = async (channelId = MFG_USER_ID) =>
  request(`streams?channel=${channelId}`)

export const getChannelInfo = async (channelId = MFG_USER_ID) =>
  request(`channels/${channelId}`)

export const getThumbnail = async (videoId, size = 'large', width, height) => {
  const info = await request(`videos/${videoId}`)

  if (!info || !info.preview) {
    return {url: ''}
  }

  let url = info.preview[size]

  if (width && height) {
    url = info.preview.template
      .replace('{width}', width)
      .replace('{height}', height)
  }

  return {url}
}
