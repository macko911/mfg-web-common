/* eslint-disable */

const TRACKING_ID = {
  faq: 'UA-40688825-9',
  uk: 'UA-40688825-8',
  'uk-contest': 'UA-40688825-14',
  sgdz: 'UA-40688825-12',
  home: 'UA-40688825-1',
  dt2: 'UA-40688825-10',
  legends: 'UA-40688825-13',
  'dt2-old': 'UA-44851924-1',
  monzo: 'UA-40688825-2',
  monzovr: 'UA-40688825-15',
  wargames: 'UA-40688825-18'
}

class Analytics {

  constructor() {
    if (typeof window === 'undefined') return
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  }

  init = (property) => {
    if (typeof window === 'undefined') return
    ga('create', TRACKING_ID[property], 'auto')
    ga('send', 'pageview')
    ga('send', 'pageview')
  }

  click = (label, category) =>
    ga('send', 'event', {
      eventCategory: category,
      eventAction: 'click',
      eventLabel: label
    })

}

export default new Analytics()
