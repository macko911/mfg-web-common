# Google Cloud Storage

> Most web assets are stored on a Google hosted file storage.

This repository contains all assets that are necessary for Madfinger Games web services.

!> For access to the repository and buckets please contact [mvobornik@madfingergames.com](mvobornik@madfingergames.com) (if he's still alive)

#### Contents

    .
    ├── mfg-home
    ├── mfg-node
    └── storage.py

Each folder represents a bucket on Google Cloud Storage under the project madfingergames-web.

#### Note on synchronization
?> For the time being it is expected from the maintainers to mirror changes on git repo and the actual Cloud Storage bucket.

The workflow should look like this:
1. Edit storage files locally
2. Pull most recent changes from remote git repository
3. Push your changes to remote git repository
4. Use Storage Cli to upload changes to the bucket


#### Asset optimizations
?> Web devs should try to serve files to users as small as possible while keeping good quality at the same time (this applies especially for images and videos, minified JSONs, etc). There is a helper function in the Cli called **optimize**, scroll down for its usage.

## Storage Cli

There is a small command-line tool in the root of this repo that helps manage content on GCS.
It is advisable to add this script to your `$PATH` so that you can use it from anywhere in the repo.

Script automatically detects current path and matches it to a cloud storage bucket. If you run the script outside of the repository, it will (read _should_) throw an error.

### Usage
```bash
storage <arguments>
```

*For example::*
```
storage upload force nocache
```

#### Upload
```bash
storage upload
```
Uploads new content from local directory to GCS. This includes new and changed files.

Additional arguments:

`clean` - deletes files from GCS that are not present in local storage (careful!)

`force` - uploads all files even if they didn't change

`nocache` - uploads files and adds header `Cache-Control:no-cache`

`status` - prints out changes that would be made using this command

#### Download
```bash
storage download
```
Downloads content from GCS that's different or non-existent in local directory.

Additional arguments:

`clean` - deletes files in local storage that are not present in GCS (careful!)

`force` - downloads all files even if they didn't change

`status` - prints out changes that would be made using this command


#### Optimizations
Uses Tinify API to make cloud storage images smaller
```bash
storage optimize
```

Additional arguments:

`<path to file>` - specify path to image file that should be optimized
