# Common module

> This module serves as a collection of utilities, React components etc. for all Madfinger Web projects.

To add this module to a new web project:
```bash
yarn add https://source.developers.google.com/p/madfingergames-web/r/mfg-web-common.git
```

!> Since this is a private repository, You have to configure git credentials as described in the [Getting Started](/getting-started) section.

To make your life easier, you should tell `yarn` to look at the development package instead of the remote package during development.

1. Download the repository source code:
```bash
git clone https://source.developers.google.com/p/madfingergames-web/r/mfg-web-common.git
```
2. Go to directory and link it:
```bash
cd mfg-web-common && yarn link
```
3. Go to any project that uses this package and link it there too:
```bash
cd ../mfg-web/admin && yarn link mfg
```
4. The `mfg` dependency in Portal should now aim at your local repository

### Contents
What's really inside the common module:

    .
    ├── cli
    ├── cloud
    ├── components
    ├── config
    ├── docs
    ├── starter
    ├── styles
    ├── utils
    ├── .eslintrc
    ├── .prettierrc
    └── app.yaml

  1.  **`/cli`**: Command-line utilities, see [cli](common/cli.md) for more info.

  2.  **`/cloud`**: Cloud requests interface, communicating with REST APIs.

  3.  **`/components`**: Shareable _React_ components between all websites.

  4.  **`/config`**: Constants such as URLs, ...

  5.  **`/docs`**: Place for the docs that you're currently reading.

  6.  **`/starter`**: When initializing a new stati website, you should copy/paste contents of this folder into your target directory

  7.  **`/styles`**: Default initial styles, including [Normalize.css](https://necolas.github.io/normalize.css/) and parts of [bulma](https://bulma.io/)

  8.  **`/.eslintrc`**: Common [eslint](https://eslint.org/) configuration for consistent coding style.

  9.  **`/.prettierrc`**: Common [prettier](https://prettier.io/) configuration for consistent coding style.

### Tips & Tricks

#### Links

Linking packages can get confusing after some time and there currently aren't convenient commands built into yarn or npm to make this easier.

To list linked packages in the current project
```bash
( ls -l node_modules ; ls -l node_modules/@* ) | grep ^l
```

To see source directories of linked packages on macOS/Linux system
```bash
ls -la ~/.config/yarn/link/
```
