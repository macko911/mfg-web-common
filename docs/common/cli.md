# 💻 CLI tool

> Command-line tools that help web guys to develop and publish web projects.

## 🤔 Help

You can find the most up-to-date description of each command and its possible options using the built-in help section in the cli itself, for example:

```bash
mfg --help
```
or
```bash
mfg deploy --help
```

## 🔨 Development

```bash
mfg dev
```

> Run servers to develop locally the webz

Currenly, this command is only applicable to the *static* websites (Unkilled, Monzo, Monzo VR).
In the background this runs 3 servers:
1. Gatsby development server at http://localhost:8000
2. Local CDN server at http://localhost:9999 (see the [cdn](/common/cli?id=local-cdn-server) command, or more on [development servers](/intro?id=servers))
3. API server at http://localhost:9080 (see the [development servers](/intro?id=servers))

## 📦 Bundling
```bash
mfg build
```

> Compile website for deployment

Compile and build code from the `/src` directory to the `/public` directory, ready for deployment to the internetz!

Internally we use `gatsby build` to build the project.

## ☁ Deployment
```bash
mfg deploy
```

> Deploy website to the internetz!

Deploys current directory to Google Cloud as an App Engine service under the specified version.

Service name is the name of the folder from which we deploy code. F.e. if we deploy from `mfg-web/admin` folder, the service is `admin`.

By default it asks a set of questions to further specify the deployment. These questions can be overridden with a set of options.

Available options:
- `--quiet` - Don't ask any questions
- `--ver` - Deploy site under specified version
- `--project` - Deploy under different project than default `madfingergames-web`
- `--config` - Specify different `app.yaml` file than the auto-generated one
- `--default` - Deploy as default version
- `--git-hash` - Prepend current git commit hash to the version name

?> This also notifies the Web team that you are performing a deployment to cloud.

## 🐿 Publishing
```bash
mfg publish
```

This is a fairly simple shortcut for running the two commands consecutively: `build` and `deploy`.

## 💁 Serving Website
```bash
mfg start
```

> Run the server that serves the website 🤷

This command gets run on the production server once the code is built and deployed. You can try it after `mfg build` to test locally if the build was successful.

## 🗄️ Local CDN server
```bash
mfg cdn
```

Run a local static file server at http://localhost:9999 which serves files located on a Cloud Storage bucket.

In order to run this server, you have to have a git clone of our cloud-storage repository in the same folder as this repository, f.e. some `/projects` folder.
```bash
git clone https://macko911@bitbucket.org/macko911/cloud-storage.git
```
