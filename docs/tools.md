# Tools

> List of awesome tools that we use in our everyday awesome lives

### Core
- git - *version-control system* [download](https://git-scm.com/downloads)
- yarn - *package manager* - [download](https://yarnpkg.com/lang/en/docs/install/#mac-stable)
- Atom - *text editor* - [download](https://atom.io/)
  - Nuclide

### Nice to have
- [tinypng.com](http://tinypng.com) - *optimize images for the web*
- Balsamiq - *wireframes made easy* - [download](https://balsamiq.com/download/)
