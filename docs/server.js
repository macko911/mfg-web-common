const express = require('express')
const path = require('path')
const open = require('open')
const lrMiddleware = require('connect-livereload')
const lrServer = require('livereload')

const app = express()
const PORT = process.env.PORT || 8080
const DOCS_PATH = path.resolve(__dirname, './')

lrServer.createServer({
  extraExts: ['md'],
  exclusions: ['node_modules/']
}).watch(DOCS_PATH)

app.use(lrMiddleware())

app.use(express.static(DOCS_PATH))

app.listen(PORT, () => {
  console.log(`Server listening @ http://localhost:${PORT}`)
  open(`http://localhost:${PORT}`)
})
