# REST API

> API server used for almost anything you can think of

Think of it as several microservices but actually glued all together in a sort of monolith monster that lives its own life and probably will outlive us all.

The documentation is created using the [OpenAPI 3.0](https://www.openapis.org/) (FKA Swagger) and available [online](https://api-dot-madfingergames-web.appspot.com/docs/) right on the API server.

!> You need to log in to see the API. If you don't know the username/password, please contact [mvobornik@madfingergames.com](mvobornik@madfingergames.com) (if he's still alive)

### Contributions

In order to edit Rest API documentation, head over to the Node server repository and look in the `/docs` folder there.
