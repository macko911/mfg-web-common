# 👨‍🎓 Getting Started

> So you want to be a MFG Web developer...

## Source code

In order to get to any source code, you need to set up git credentials.

1. Go to https://source.developers.google.com/new-password
2. Copy/paste code into your console according to instructions
3. Now you should be able to get to the source code:
```bash
git clone https://source.cloud.google.com/madfingergames-web/mfg-web
```

✅  Congratulations! You are now an official MFG Web Developer™️!


## Repositories

All repositories are stored on [Google Source Repositories](https://cloud.google.com/source-repositories/).
```bash
https://source.cloud.google.com/madfingergames-web/<repository-name>
```

1. **`mfg-web`**: Public websites and internal tools
2. **`mfg-web-common`**: Common code for all web projects
3. **`mfg-cloud-storage`**: Static assets such as images and videos

## Websites

All websites are hosted on Google Cloud platform. This not only gives us slow deployment 🐢 but also high prices!🤑

Company web  
https://www.madfingergames.com  
`mfg-web/home`  
> This website also hosts "sub-websites"  
https://www.madfingergames.com/deadzone/  
mfg-web/home/sections/deadzone  
https://www.madfingergames.com/deadtrigger2/  
mfg-web/home/sections/dt2  

Shadowgun Wargames  
https://www.shadowgunwargames.com  
`mfg-web/home/sections/wargames`

Shadowgun Legends  
https://www.shadowgunlegends.com  
`mfg-web/home/sections/legends`

Unkilled  
https://www.unkilledgame.com  
`mfg-web/static/unkilled`

Monzo  
https://www.monzoapp.com  
`mfg-web/static/monzo`

Monzo VR  
https://www.monzovr.com  
`mfg-web/static/monzovr`

MFG Portal  
Internal app that helps people with their lives, see [here](/admin/about) for details.
https://portal.madfingergames.com  
`mfg-web/admin`  

## Servers

![servers](/img/servers.jpg)

?> There are so many development servers that it almost looks like there is a design flaw in the development environment. While this still might be true, I am of the opinion that splitting development servers into several smaller ones increases modularity and actually simplifies the development process. Feel free to disagree and rewrite everything.😉

#### 1. Home web server

> Handles server-side rendering (SSR) of React application and serves static content (usually legacy assets and micro-sites)

|Port|8080|
|--|--|
|location|`mfg-web/home/server`|
|command|`yarn dev`|

#### 2. API server

> Handles various requests such as MFG Cloud communication

|Port|9080|
|--|--|
|location|`mfg-web/api/src`|
|command|`yarn dev`|

#### 3. CDN server

> Serves static files from local mfg-cloud-storage repository

|Port|9999|
|--|--|
|location|`mfg-cloud-storage`|
|command|`mfg cdn`|

#### 4. Admin server

> Serves Admin tool React bundle & authentication

|Port|6060|
|--|--|
|location|`mfg-web/admin`|
|command|`yarn dev`|
