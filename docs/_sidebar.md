* [Home](/)

* [Getting Started](getting-started.md)

* [Public websites](public.md)

* [Common Module](common/about.md)

* [Cli](common/cli.md)

* [Portal (fka Admin tool)](admin/about.md)

* [REST API](rest-api.md)

* MFG Cloud APIs

  * [HR Tool](cloud/hr-tool.md)

* [Cloud Storage](storage.md)

* [Tools](tools.md)

* [Backlog](backlog.md)
