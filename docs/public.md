# Public websites

> bla bla bla TODO

## Dynamic websites

> Websites served with a "complex" Express server with SSR functionality

Dynamic websites are located in the `mfg-web/home` folder. Currently these are:
- Company - https://www.madfingergames.com
- Legends - https://www.shadowgunlegends.com
- Wargames - https://www.shadowgunwargames.com

### Contents

What's really inside the `/home` folder:

    .
    ├── build
    ├── client
      ├── components
      ├── config
      ├── ext
      ├── lang
      ├── sections
      ├── store
      ├── styles
      ├── utils
      ├── createApp.js
      ├── index.js
      └── intl.js
    ├── public
    ├── scripts
    ├── server
    ├── webpack
    ├── .env
    ├── .eslintrc
    ├── .flowconfig
    ├── .gcloudignore
    ├── .prettierrc
    ├── app.yaml
    ├── babel.config.js
    ├── postcss.config.json
    └── sentry.properties

  1.  **`/build`**: Bundled React app serving SSR (Server-side rendering)

  2.  **`/client`**: All code visible on the front-end  

    1. **`/components`**: React components

    2. **`/config`**: Reusable constants, f.e. path to API server

    2. **`/ext`**: External libraries, f.e. Google Analytics

    2. **`/lang`**: Localization texts stored in JSON format

    2. **`/sections`**: A mix of pages and all websites, f.e. /support, /jobs, /dt2, shadowgunlegends.com, etc.

    2. **`/store`**: Redux store

    2. **`/styles`**: SASS styles

    2. **`/utils`**: Common code for client app

    2. **`/createApp.js`**: Entry file for server React

    2. **`/index.js`**: Entry file for client React

    2. **`/intl.js`**: react-intl configuration file

  3.  **`/public`**: Bundled code-split React app for front-end and other assets served by the dedicated Node server.

  4.  **`/scripts`**: Cli tools specific for the home project _(might be moved to common module eventually.)_

  5.  **`/server`**: Dedicated Node server handling SSR and proxying API requests to dedicated API server.

  6.  **`/webpack`**: Webpack configurations for both client and server bundles.

  7.  **`/.env`**: Super secret secrets. Don't look!

  8.  **`/.eslintrc`**: Common [eslint](https://eslint.org/) configuration for consistent coding style.

  9.  **`/.flowconfig`**: This file enables Flow in the project 🙂

  10.  **`/.gcloudignore`**: Specify files/folders to ignore during deployment

  11.  **`/.prettierrc`**: Common [prettier](https://prettier.io/) configuration for consistent coding style.

  12.  **`/app.yaml`**: Deployment configuration file

  12.  **`/babel.config.js`**: Babel transformation plugins

  12.  **`/postcss.config.js`**: CSS post-processing plugins

  12.  **`/sentry.properties`**: Config for error reporting tool [Sentry.io](https://sentry.io/)


### Scripts

> These scripts are only applicable to the home project and are located conviniently in the `/scripts` folder. They might be moved to the common module when the right time comes.

#### 🔨 Development
```bash
yarn dev
```
Start up React, API and CDN servers. It asks you which website you want to currently develop.
#### 📦  Bundling
```bash
yarn build
```
Start up Webpack and build Client and Server bundle.
```bash
yarn build:client
```
Only build Client bundle
```bash
yarn build:server
```
Only build Server bundle
#### 📠  Servers
```bash
yarn start
```
Start up server in PRODUCTION mode.  
You need to re-build client and server Webpack bundle to see the latest changes.  
Good for testing right before deployment.
```bash
yarn start:dev
```
Start up server in DEVELOPMENT mode.  
This is basically only needed when tweaking the SSR process or redirects.
#### ☁ Deployment
```bash
yarn deploy
```
Send built code to Google Cloud App Engine `home` service.
#### 🔍 Fonts
```bash
yarn fonts
```
Icon bundling optimization. Search client code for used Font Awesome icons and import them separately instead of improting all available FA icons. This is run before the webpack bundling command.

?> Let's face it...  This has almost nothing to do with fonts 🤷‍
#### 🇨🇳 Localizations
```bash
yarn localize <website> [options]
```
Localization tool to download, upload and synchronize localized texts with external database.
```bash
yarn localize legends --sync
```
Synchronize local language changes with cloud.
#### 🐿 Publish
```bash
yarn run publish
```
Basically a shortcut to `build` and `deploy` the project.


## Static websites

> Websites served with a simple Express server

Static websites are located in the main repository under the `mfg-web/static` folder. Currently these are:
- Monzo - https://www.monzoapp.com
- Monzo VR - https://www.monzovr.com
- Unkilled - https://www.unkilledgame.com

Internally we are using [Gatsby.js](https://www.gatsbyjs.org/) to build static websites from React components.

All static websites have a starting boilerplate located in the `mfg-web-common/starter` repository.

### SSL Certificates

All SSL certificates are auto-managed by Google and can be viewed in the [Google Console](https://console.cloud.google.com/appengine/settings/domains?project=madfingergames-web&organizationId=307742938408)

Exception is the wildcard certificate for [\*.madfingergames.biz]() that is used to access Get Response under a secure url. It needs to be a wildcard certificate for some internal reasons.
