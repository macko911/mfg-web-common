# HR Tool cloud implementation (draft)

## List available positions
```
GET /positions/list
```
Returns a list of all available positions along with basic info to show on the positions overview page.

> NOTE: Since this is not a huge amount of data, I would probably fetch them all together rather than deal with pagination.

Reponse:
```javascript
[{
  id: 'fnc732fl',
  name: 'UI/UX Graphic Designer',
  state: 'open',
  candidates: {
    total: 12,
    new: 4,
    pending: 2
  },
  opened: '2018-10-08 00:00:00',
  author: 'mvobornik@madfingergames.com',
  team: []
}, ... ]
```

- The `candidates` field internally would be an array of candidate IDs, but the endpoint calculates the amount of total, new and pending candidates.

| name  | description  |
|---|---|
| total | all assigned candidates |
| new | candidates with state set to New |
| pending | state is not New but they are waiting for response from HR -> last conversation message is from candidate (see candidate detail endpoint).|

- The `state` field can be one of: `draft`, `open`, `closed`.

- The `id` field should be a simple short string because it is visible in the browser url.

## Get position detail
```
GET /position/{position-id}
```
Returns all available info for a specified position.

Reponse:
```javascript
{
  name: 'UI/UX Graphic Designer',
  state: 'open',
  candidates: [
    '0axk5qlxm7',
    'v9tl3t21dj',
    'a3o7yyh9x1'
  ],
  opened: '2018-10-08 00:00:00',
  author: 'mvobornik@madfingergames.com',
  team: [
    'bkuchta@madfingergames.com',
    'eferencz@madfingergames.com'
  ],
  id: 'fnc732fl',
  manager: 'babec@madfingergames.com',
  department: 'Founders',
  startDate: '2018-12-01T08:44:03.161Z',
  salary: '50 - 60 000',
  hiringReason: 'new position',
  contract: 'full time',
  justification: 'test justification',
  description: {
    summary: '<p>test text</p>',
    responsibilities: '<p>test text</p>',
    skills: '<p>test text</p>',
    skills2: '<p>test text</p>'
  },
  objectives: {
    top3: 'test text',
    expected: 'test text',
    challenges: 'test text',
    junior: 'test text'
  },
  sellingPoints: {
    positives: 'test text',
    improvements: 'test text',
    branding: 'test text'
  },
  publicDescription: '<p>test text</p>'
}
```

## Create position
```
POST /position/{position-id}
```

Request Body:
```javascript
{
  name: 'UI/UX Graphic Designer',
  author: 'mvobornik@madfingergames.com'
}
```

- only `id` and `name` are required

- Returns conflict if `id` or `name` is already taken.

- `state` is automatically set to `draft`

## Edit position
```
PUT /position/{position-id}
```

Request Body:
```javascript
{
  name: 'Web master'
}
```

## Fetch all candidates
```
GET /candidates/list
```

Returns a list of candidates

Query params:

|name|*default*|description|
|-|-|-|
|cursor|*-*|fetch candidates from given cursor|
|limit|*20*|fetch number of candidates|
|sort|*update date (descending)*|fetch candidates sorted by property [update/create date, name]|

Reponse:
```javascript
{
  result: [{
    name: 'Petr Gonzalez',
    id: '0axk5qlxm7',
    created: '2018-03-30T11:02:20+02:00',
    updated: '2018-08-05T15:07:32+02:00',
    state: 'accepted',
    positions: [
      'fnc732fl'
    ]
  }, ... ],
  nextCursor: 'nmc8723nmv8kjhbnmsdv8723msd9jksd',
  prevCursor: null
}
```

## Fetch candidate detail

```
GET /candidate/{candidate-id}
```

Returns all available info for a specified position.

Reponse:
```javascript
{
  name: 'Petr Gonzalez',
  id: '0axk5qlxm7',
  created: '2018-03-30T11:02:20+02:00',
  updated: '2018-08-05T15:07:32+02:00',
  positions: [
    'fnc732fl'
  ],
  state: 'accepted',
  general: {
    email: 'petrgonzalez@gmail.com',
    phone: '+420607629991',
    link: 'linkedin.com/petrgonzales',
    custom: 'field'
  },
  source: 'web',
  files: [
    'https://storage.googleapis.com/mfg-home/admin/hr/candidate/0axk5qlxm7/legends.png'
  ],
  screening: {
    background: 'test text',
    motivation: 'test text',
    games: 'test text',
    english: 4,
    position: 'test text',
    current: 'test text',
    jobChange: 'test text',
    skills: 'test text',
    years: 'test text',
    education: 'test text',
    startDate: 'test text',
    location: 'test text',
    relocate: 'test text',
    salary: 'test text'
  },
  conversation: [
    {
      id: 'u1m8gd2txa',
      text: 'Nam placerat tempus sollicitudin...',
      from: '0axk5qlxm7',
      date: '2017-08-23T10:43:12+02:00',
      attachments: []
    },
    {
      id: '0k07pg4g2q',
      text: 'I play ukulele and nerves. Also very good in basejumping and paperplane folding.',
      from: 'vvecerova@madfingergames.com',
      date: '2017-03-01T00:53:43+01:00',
      attachments: [
        'https://storage.googleapis.com/mfg-home/Porn.exe'
      ]
    }
  ],
  notes: [
    {
      id: 'tr83f314a4',
      text: 'Hello, I want a job. I am very good...',
      from: 'zvodstrcilova@madfingergames.com',
      date: '2018-04-05T16:20:19+02:00',
      attachments: [
        'https://storage.googleapis.com/mfg-home/Pizza-delivery-receipt.pdf'
      ]
    }
  ],
  history: [
    {
      date: '2018-02-20T00:29:42+01:00',
      type: 'created'
    }
  ]
}
```

- `state` can be one of `new`, `screening`, `submitted to manager`, `test assigned`, `pending feedback`, `interview scheduled`, `job offer`, `accepted`, `declined`.

- `screening.english` is `int` on scale from `0 - 4`.

- `notes` and `conversation` objects have the same properties.

## Send candidate message

```
POST /candidate/{candidate-id}/message
```
Request body:
```javascript
{
  text: '<p>Hello world</p>',
  attachments: [
    'https://storage.googleapis.com/mfg-home/Pizza-delivery-receipt.pdf'
  ]
}
```

- `text` property has HTML syntax
- `attachments` are already uploaded to Cloud Storage before the message gets sent

## Send mass email to candidates
```
POST /candidates/message
```
Send the same email to a specified amount of candidates

> For example, *"Sorry we have chosen someone else"*, could be around 3 - 20 recipients.

Query params:

|name|*default*|description|
|-|-|-|
|recipients|*- (required)*| list of email addresses|

Request body:
```javascript
{
  text: '<p>Hello world</p>',
  attachments: [
    'https://storage.googleapis.com/mfg-home/Pizza-delivery-receipt.pdf'
  ]
}
```
- `text` property has HTML syntax
- `attachments` are already uploaded to Cloud Storage before the message gets sent
